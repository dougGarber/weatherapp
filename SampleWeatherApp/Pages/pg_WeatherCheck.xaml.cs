﻿using System;
using System.Collections.Generic;
using System.Net.Cache;
using Newtonsoft.Json;
using System.Net.Http;
using Xamarin.Forms;
using SampleWeatherApp.Models;



namespace SampleWeatherApp
{
    public partial class pg_WeatherCheck : ContentPage
    {
        //url construction and params
        private const string baseUrl = "https://api.openweathermap.org/data/2.5/weather?";
        private const string paramUnits = "&units=imperial";
        private const string paramAppID = "&APPID=cf87d3a95ce7a3d4f2460911fb516c46";
        private const string paramCity = "q=";
        private const string paramCountry = "us";

        //init httpClient for data calls
        private HttpClient _Client = new HttpClient();


        public pg_WeatherCheck()
        {
            InitializeComponent();
        }

        /**get weather button click**/
        private async void GetWeather_OnClicked(object sender, EventArgs e)
        {

            //determine if a city name was entered
            if (String.IsNullOrEmpty(txtCity.Text))
            {
                //city name not entered so display messaging
                grdData.IsVisible = false;
                txtNoCity.IsVisible = true;
                txtNotFound.IsVisible = false;
            }
            else
            {
                //city name entered so display grid
                grdData.IsVisible = true;
                txtNoCity.IsVisible = false;
                txtNotFound.IsVisible = false;

                //display activity spinner
                activity.IsEnabled = true;
                activity.IsRunning = true;
                activity.IsVisible = true;

                //construct api url
                var url = baseUrl + paramCity + txtCity.Text + "," + paramCountry + paramUnits + paramAppID;
                try
                {
                    //attempt to call api url and format data
                    var content = await _Client.GetStringAsync(url);
                    var data = JsonConvert.DeserializeObject<WeatherModel>(content);

                    //data display grid
                    txtDescr.Text = "Current conditions in " + txtCity.Text;
                    txtDt.Text = UnixTimeStampToDateTime(data.dt);
                    txtTemp.Text = FormatTemperature(data.main.temp);
                    txtPrecip.Text = FormatPrecipitation(data.rain);

                    //hide activity spinner
                    activity.IsEnabled = false;
                    activity.IsRunning = false;
                    activity.IsVisible = false;

                }
                catch (Exception)
                {
                    //404 - city does not exist
                    grdData.IsVisible = false;
                    txtNoCity.IsVisible = false;
                    txtNotFound.IsVisible = true;

                    //hide activity spinner
                    activity.IsEnabled = false;
                    activity.IsRunning = false;
                    activity.IsVisible = false;
                }

            }
        }

        /** convert unix date-timestamp to local date-timestamp display value **/
        private static string UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime.ToString();
        }

        /** format temperature display value **/
        private static string FormatTemperature(double temperature)
        {
            return temperature.ToString() + "\u00B0 F";
        }

        /** format precipitation display value **/
        private static string FormatPrecipitation(SampleWeatherApp.Models.Rain precip)
        {
             if (precip != null)
             {
               return precip.h + "in 1 hr"; 
             }
             else
             {
               return "0.0in 1 hr";
             }
        }
    }
}